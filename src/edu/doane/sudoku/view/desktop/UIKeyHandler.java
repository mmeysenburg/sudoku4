package edu.doane.sudoku.view.desktop;

import edu.doane.sudoku.controller.DesktopController;
import edu.doane.sudoku.controller.SuDoKuController;
import java.awt.KeyEventDispatcher;
import java.awt.event.KeyEvent;

/**
 * Class to handle key input in the desktop SuDoKu app.
 *
 * @author Mark M. Meysenburg
 * @version 12/16/2015
 */
public class UIKeyHandler implements KeyEventDispatcher {

    /**
     * Flag telling if we're in note entry mode or not.
     */
    private boolean notesMode;

    /**
     * boolean to determine if game is paused or not
     */
    private boolean b;

    /**
     * variable to keep track of whether the game is paused or not
     */
    private boolean acceptsInput = true;

    /**
     * Reference to the cells in the user interface.
     */
    private final UICell[][] cells;

    /**
     * Reference to the controller used by the app.
     */
    private final SuDoKuController controller;

    /**
     * Reference to the status bar.
     */
    private final UIStatusBar pnlStatusBar;

    /**
     * Construct the key handler.
     *
     * @param cells User interface cells
     * @param controller Reference to the application's controller
     * @param pnlStatusBar User interface status bar
     */
    public UIKeyHandler(UICell[][] cells, SuDoKuController controller, UIStatusBar pnlStatusBar) {
        this.cells = cells;
        this.controller = controller;
        notesMode = false;
        this.pnlStatusBar = pnlStatusBar;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        // see what character was typed
        char c = e.getKeyChar();
        if (!controller.getIsLocked()) {
            if (acceptsInput) {

                // and handle the input
                switch (c) {

                    // n toggles notes mode
                    case 'n':
                    case 'N':
                        notesMode = !notesMode;
                        setNotesOrNormal();

                        break;

                    // 1 - 9 sets number or note
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        if (notesMode) {
                            setNote(c);
                        } else {
                            setNumber(c);
                        }
                        break;

                    // space clears number from cell
                    case ' ':
                        if (!notesMode) {
                            setNumber(' ');
                        }
                        break;

                    // p toggles paused mode
                    case 'p':
                    case 'P':
                        if (acceptsInput == true) {
                            acceptsInput = false;
                            DoaneSuDoKu.getController().isPaused(!acceptsInput);
                        }//end of if the game is paused
                        else {
                            acceptsInput = true;
                            DoaneSuDoKu.getController().isPaused(!acceptsInput);
                        }//end of if the game isn't paused
                        break;
                }//end of switch(c)
            } else {
                if (c == 'P' || c == 'p') {
                    if (acceptsInput == true) {
                        acceptsInput = false;
                        DoaneSuDoKu.getController().isPaused(!acceptsInput);
                    }//end of if the game is paused
                    else {
                        acceptsInput = true;
                        DoaneSuDoKu.getController().isPaused(!acceptsInput);
                    }//end of if the game isn't paused
                }
            }
        }

        return false;
    }

    /**
     * Toggle between notes and normal mode
     */
    private void setNotesOrNormal() {
        // update cells so they can display the correct image
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                if (notesMode) {
                    cells[row][col].setNotesMode();
                } else {
                    cells[row][col].setNormalMode();
                }
                cells[row][col].repaint();
            }
        }
        // update status bar so it indicates notes mode status
        if (notesMode) {
            pnlStatusBar.setNotesMode();
        } else {
            pnlStatusBar.setNormalMode();
        }
    }

    /**
     * Cause a note to be set in the selected cell.
     *
     * @param c Character holding number value to set.
     */
    private void setNote(char c) {
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                if (cells[row][col].isSelected()) {
                    // ask the controller to set the note
                    controller.setNote(row, col, Integer.parseInt(Character.toString(c)));

                    // get us out of the loop once the note has been set
                    break;
                }
            }
        }
    }

    /**
     * Cause a number to be set in the selected cell.
     *
     * @param c Character holding the number value to set.
     */
    private void setNumber(char c) {
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                if (cells[row][col].isSelected()) {
                    if (c == ' ') {
                        // ask the controller to remove the number
                        controller.removeNumber(row, col);
                    } else {
                        // ask the controller to set the number
                        controller.playNumber(row, col, Integer.parseInt(Character.toString(c)));
                    }

                    // get us out of the loop once the number has been set
                    break;
                }
            }
        }
    }

    public void setAcceptsInput(boolean b) {
        acceptsInput = b;
    }
}
