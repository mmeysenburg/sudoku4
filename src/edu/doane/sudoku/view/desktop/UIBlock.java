package edu.doane.sudoku.view.desktop;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import javax.swing.JPanel;

/**
 * Custom JPanel representing a 3x3 block of cells in the desktop app's UI.
 * 
 * @author Mark M. Meysenburg
 * @version 12/16/2015
 */
public class UIBlock extends JPanel {
    
	/**
	 * Silence the Eclipse warning about serialization, even though
	 * we're not doing that. 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create a new UIBlock.
	 */
	public UIBlock() {
		// make it a JPanel first
        super();
        
        // background is black, with a gap between the cells in the grid;
        // this automatically draws thin black border lines between each
        // cell 
        this.setBackground(Color.BLACK);
        GridLayout gl = new GridLayout(3, 3);
        gl.setHgap(2);
        gl.setVgap(2);
        this.setLayout(gl);
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
    }
    
}
