package edu.doane.sudoku.view.desktop;

import com.sun.xml.internal.ws.util.StringUtils;
import edu.doane.sudoku.controller.BlockObservable;
import edu.doane.sudoku.controller.DesktopTimer;
import edu.doane.sudoku.controller.DesktopController;
import edu.doane.sudoku.controller.SuDoKuController;
import edu.doane.sudoku.view.SuDoKuUI;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.KeyboardFocusManager;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import java.util.Observer;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * User interface for the desktop SuDoKu application.
 *
 * @author Mark M. Meysenburg
 * @version 12/16/2015
 */
public class DoaneSuDoKu implements SuDoKuUI {

    /**
     * JFrame window that is the face of the application.
     */
    private JFrame frame;

    /**
     * JFrame window that is the pause screen of the application
     */
    private JFrame pause;

    /**
     * 9x9 array of UICells, displaying the numbers / notes of the game.
     */
    private UICell[][] cells;

    /**
     * JPanel holding a 3x3 grid of blocks, each of which holds a 3x3 block of
     * cells.
     */
    private UIGrid grid;

    /**
     * 3x3 grid of blocks that go on the grid.
     */
    private UIBlock[][] blocks;

    /**
     * Status bar holding timer, notes mode status, etc.
     */
    private UIStatusBar statusBar;

    /**
     * Controller used by the app.
     */
    private static SuDoKuController controller;

    /**
     * Audio playing object for game sound effects.
     */
    private DesktopAudio audioSystem;

    private int bounds[][] = {{0, 2, 0, 2},
    {0, 2, 3, 5},
    {0, 2, 6, 8},
    {3, 5, 0, 2},
    {3, 5, 3, 5},
    {3, 5, 6, 8},
    {6, 8, 0, 2},
    {6, 8, 3, 5},
    {6, 8, 6, 8}};

    private String[] puzzleImg = new String[81];

    /**
     * Default constructor for the app. Configures the frame, turns on keystroke
     * capturing, etc.
     */
    public DoaneSuDoKu() {

        configureFrame();       // set up the JFrame window

        // create the controller for the app
        controller = new DesktopController(this, new DesktopTimer());

        configureMenu();        // set up the menus

        captureKeyStrokes();    // enable kestroke capture

        // get the audio player up and running
        audioSystem = DesktopAudio.getInstance();
        
        setPuzzleImg("shield.txt");
        
        setUICellColor();

    }

    /**
     * Configure one of the JPanels that holds a 3x3 block of cells.
     *
     * @param row Row of the block
     * @param col Column of the block
     */
    private void buildBlock(int row, int col) {
        blocks[row][col] = new UIBlock();
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                blocks[row][col].add(cells[row * 3 + r][col * 3 + c]);
            }
        }
    }

    /**
     * Create the 9x9 array of UICells displaying the numbers / notes in the
     * game.
     */
    private void buildCells() {
        cells = new UICell[9][9];
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                cells[row][col] = new UICell();
            }
        }
    }

    /**
     * Create the JPanels that hold the cells and provide the lines between the
     * cells and blocks of cells.
     */
    private void buildGrid(Container pane) {
        grid = new UIGrid();
        pane.add(grid, BorderLayout.CENTER);

        blocks = new UIBlock[3][3];
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                buildBlock(row, col);
                grid.add(blocks[row][col]);
            }
        }
    }

    /**
     * Create the status bar and add it to the interface.
     *
     * @param pane Container on the app's JFrame.
     */
    private void buildStatusBar(Container pane) {
        statusBar = new UIStatusBar();
        pane.add(statusBar, BorderLayout.SOUTH);
    }

    /**
     * Configure the appearance of the JFrame window for the application.
     */
    private void configureFrame() {
        frame = new JFrame("Doane SuDoKu");

        frame.pack();

        Container pane = frame.getContentPane();
        pane.setLayout(new BorderLayout());

        buildCells();

        buildGrid(pane);

        buildStatusBar(pane);

        frame.setSize(700, 700);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            //@override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                controller.getTimer().stopTimer();
                if (confirmExit()) {
                    System.exit(0);
                }
                controller.getTimer().startTimer();
            }
        });
        frame.setVisible(true);

        //JFrame for when the game is paused
        pause = new JFrame("Pause SuDoKu");
        pause.pack();
        Container pPane = pause.getContentPane();

        pause.setSize(700, 700);
        pause.setLocationRelativeTo(frame);
        
        String padded = ("                                                                                          ");
        pPane.add(new JLabel(padded), BorderLayout.WEST);
        pPane.add(new JLabel("PRESS P TO UNPAUSE"), BorderLayout.CENTER);
        pause.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pause.setVisible(false);
    }

    /**
     * obfuscate the screen when the game is paused
     *
     * (display a paused jframe and hide the game jframe)
     *
     * @param i
     */
    public void obfuscate(int i) {
        //if the game is not paused/unobfuscated
        if (i == 0) {
            pause.setLocationRelativeTo(frame);
            pause.setVisible(true);
            frame.setVisible(false);
        }//end of if the game is not obfuscated

        //if the game is paused/obfuscated
        if (i == 1) {
            frame.setLocationRelativeTo(pause);
            pause.setVisible(false);
            frame.setVisible(true);
        }//end of if the game is obfuscated

    }//end of obfuscate

    /**
     * Set up the menu system for the application.
     */
    private void configureMenu() {
        UIMenuHandler mnuHandler = new UIMenuHandler(controller);

        JMenuBar menuBar = new JMenuBar();

        JMenu gameMenu = new JMenu("Game");
        gameMenu.setMnemonic('G');

        JMenuItem gameNewGameMenuItem = new JMenuItem("New game");
        gameNewGameMenuItem.setMnemonic('N');
        gameMenu.add(gameNewGameMenuItem);
        gameNewGameMenuItem.addActionListener(mnuHandler);

        JMenuItem gameClearGridMenuItem = new JMenuItem("Clear grid");
        gameClearGridMenuItem.setMnemonic('C');
        gameMenu.add(gameClearGridMenuItem);
        gameClearGridMenuItem.addActionListener(mnuHandler);

        gameMenu.addSeparator();
        JMenuItem gameExitMenuItem = new JMenuItem("Exit");
        gameExitMenuItem.setMnemonic('x');
        gameMenu.add(gameExitMenuItem);
        gameExitMenuItem.addActionListener(mnuHandler);

        menuBar.add(gameMenu);

        JMenu helpMenu = new JMenu("Help");
        helpMenu.setMnemonic('H');

        helpMenu.addSeparator();
        JMenuItem helpAboutMenuItem = new JMenuItem("About...");
        helpAboutMenuItem.setMnemonic('A');
        helpMenu.add(helpAboutMenuItem);
        helpAboutMenuItem.addActionListener(mnuHandler);

        helpMenu.addSeparator();
        JMenuItem helpInstructionsMenuItem = new JMenuItem("Instructions...");
        helpInstructionsMenuItem.setMnemonic('I');
        helpMenu.add(helpInstructionsMenuItem);
        helpInstructionsMenuItem.addActionListener(mnuHandler);

        menuBar.add(helpMenu);

        frame.setJMenuBar(menuBar);
    }

    /**
     * Turn on keyboard capture for the application, so numbers / notes can be
     * entered.
     */
    private void captureKeyStrokes() {
        UIKeyHandler kh = new UIKeyHandler(cells, controller, statusBar);
        KeyboardFocusManager.getCurrentKeyboardFocusManager()
                .addKeyEventDispatcher(kh);

    }

    /**
     * Set up the platform-independent "Nimbus" look and feel, so the app looks
     * the same on all systems. More or less.
     */
    private static void setLookAndFeel() {
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException
                | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(DoaneSuDoKu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Application entry point.
     *
     * @param args Command-line arguments; ignored by this application.
     */
    public static void main(String[] args) {

        // uncomment if the Nimbus look and feel is desired. Leave as is
        // for platform-specific looks on Windows, Mac, and Linux platforms. 
//        setLookAndFeel();
        java.awt.EventQueue.invokeLater(() -> {
            final DoaneSuDoKu app;
            app = new DoaneSuDoKu();
        });
    }

    @Override
    public void clearGrid(boolean newGame) {
        if (audioSystem != null) {
            if (!newGame) {
                audioSystem.playClearGrid();
            } else {
                audioSystem.playNewGame();
            }
        }

        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                if (newGame) {
                    cells[row][col].unsetGiven();
                }
                cells[row][col].clearNumber();
                cells[row][col].clearAllNotes();
            }
        }
    }

    @Override
    public void setDifficulties(String[] difficulties) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setGiven(int row, int col, int number) {
        char n = '0';
        switch (number) {
            case 1:
                n = '1';
                break;
            case 2:
                n = '2';
                break;
            case 3:
                n = '3';
                break;
            case 4:
                n = '4';
                break;
            case 5:
                n = '5';
                break;
            case 6:
                n = '6';
                break;
            case 7:
                n = '7';
                break;
            case 8:
                n = '8';
                break;
            case 9:
                n = '9';
                break;
        }

        cells[row][col].setGiven(n);
    }

    @Override
    public void setNumber(int row, int col, int number) {
        if (number >= 1 && number <= 9) {
            audioSystem.playPlayNumber();
        } else if (number == 0) {
            audioSystem.playEraseNumber();
        }

        char n = '0';
        switch (number) {
            case 0:
                n = '0';
                break;
            case 1:
                n = '1';
                break;
            case 2:
                n = '2';
                break;
            case 3:
                n = '3';
                break;
            case 4:
                n = '4';
                break;
            case 5:
                n = '5';
                break;
            case 6:
                n = '6';
                break;
            case 7:
                n = '7';
                break;
            case 8:
                n = '8';
                break;
            case 9:
                n = '9';
                break;
        }

        cells[row][col].setNumber(n);
    }

    @Override
    public void setTimerValue(String value) {
        statusBar.setTime(value);
    }

    @Override
    public void celebrate(int id, String time) {
        audioSystem.playCelebrate();
        JOptionPane.showMessageDialog(frame, "Congratulations! You won game #"
                + id + " in " + time + "!", "Winner", JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void toggleNote(int row, int col, int number) {
        audioSystem.playNoteToggle();
        char c = (char) ('0' + number);
        cells[row][col].setNote(c);
    }

    @Override
    public boolean confirmExit() {
        int res = JOptionPane.showConfirmDialog(frame, "Quit?",
                "Exit Doane SuDoKu?", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        return res == JOptionPane.YES_OPTION;
    }

    @Override
    public boolean confirmNewGame() {
        int res = JOptionPane.showConfirmDialog(frame, "New game?",
                "Begin a new game?", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        return res == JOptionPane.YES_OPTION;
    }

    @Override
    public void displayAbout() {

        JPanel panel = new JPanel();
        frame.add(panel);
        final JButton button = new JButton("Sudoku guide for beginners and experienced players");
        panel.add(button);

        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                String url = "http://www.instructables.com/id/Sudoku%3Asolving-it-for-beginners-and-the-expirience/";
                if (source == button) {
                    try {
                        java.awt.Desktop.getDesktop().browse(java.net.URI.create(url));
                    } catch (IOException ex) {
                        Logger.getLogger(DoaneSuDoKu.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

        JOptionPane.showMessageDialog(frame, button, "About SuDoKu", JOptionPane.INFORMATION_MESSAGE);

    }

    @Override
    public void displayInstructions() {

//        JLabel label= new JLabel("Instructions: \n"copyright ",
//                JLabel.CENTER);
        JOptionPane.showMessageDialog(frame, "Instructions for SuDoKu:\nn - switch to or out of notes mode\nwhile in notes mode, click the same number to erase the number\nspace bar - erase current cell number\np - pause\ncopyright"
                + "\u00A9" + "100",
                "Instructions for Sudoku",
                JOptionPane.INFORMATION_MESSAGE);

    }

    public static SuDoKuController getController() {
        return controller;
    }

    public void setPuzzleImg(String img) {
        try {
            FileInputStream file = new FileInputStream(img);
            Scanner fileIn = new Scanner(file);
            int index = 0;
            while (fileIn.hasNext()) {
                puzzleImg[index] = fileIn.nextLine();
                index++;
            }
        }catch (FileNotFoundException fnfe) {
            System.out.println("Invalid bit image file");
        }
    }

    public void setObservers(BlockObservable block, int i) {
        for (int row = bounds[i][0]; row <= bounds[i][1]; row++) {
            for (int col = bounds[i][2]; col <= bounds[i][3]; col++) {
                cells[row][col].setBlockObservable(block);
            }
        }
    }

    public void setUICellColor() {
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                cells[row][col].setBitColor(puzzleImg[9 * row + col]);
            }
        }
    }

    public void clearBackground() {
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                cells[row][col].setSolved(false);
            }
        }
    }
}
