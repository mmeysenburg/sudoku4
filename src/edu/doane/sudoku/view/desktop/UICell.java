package edu.doane.sudoku.view.desktop;

import edu.doane.sudoku.controller.BlockObservable;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * Custom JPanel representing a cell in a desktop SuDoKu game.
 *
 * @author Mark M. Meysenburg
 * @version 12/15/2015
 */
public class UICell extends JPanel implements MouseListener, Observer {

    /**
     * Silence the Eclipse warning about serialization, even though we're not
     * doing that.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Reference to the image being displayed as the background of the cell
     */
    private BufferedImage image;

    /**
     * Image shown when the cell is not selected. Note this is a static class
     * variable, so there's only one instance of the image, displayed multiple
     * times on the UI.
     */
    private static BufferedImage normal;

    /**
     * Static class variable, referencing the image shown when the cell is
     * selected for number entry.
     */
    private static BufferedImage selected;

    /**
     * Static class variable, referencing the image shown when the cell is
     * selected for notes entry.
     */
    private static BufferedImage noteImg;

    /**
     * Static class variable, referencing the font used for displaying numbers
     */
    private static Font numberFont;

    /**
     * Static class variable, referencing the font used for displaying notes
     */
    private static Font noteFont;

    /**
     * Array holding notes status for numbers 1 through 9
     */
    private boolean[] notes;

    /**
     * String holding number displayed in the cell
     */
    private String num;

    /**
     * Flag indicating notes mode or normal number mode.
     */
    private boolean notesMode;

    /**
     * Flag indicating if this cell holds a given or not.
     */
    private boolean isGiven;

    /**
     * True if the mouse pointer is over this cell and we're not in notes mode.
     */
    private boolean isSelected;

    ////////////////////////////////////////////////////////////////////////////
    private boolean blockSolved = false;

    private Color bitColor;

    private Observable blockObservable;
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Create a cell for the specified location in the grid.
     *
     */
    public UICell() {
        super();

        // create and initialize notes array; note location 0 is not used
        notes = new boolean[10];
        for (int i = 0; i < notes.length; i++) {
            notes[i] = false;
        }

        // load images if they haven't already been read
        try {
            if (normal == null) {
                normal = ImageIO.read(ClassLoader.getSystemResource(
                        "resources/images/normal.png"));
            }
            if (selected == null) {
                selected = ImageIO.read(ClassLoader.getSystemResource(
                        "resources/images/selected.png"));
            }
            if (noteImg == null) {
                noteImg = ImageIO.read(ClassLoader.getSystemResource(
                        "resources/images/notes.png"));
            }

        } catch (IOException ex) {
            System.err.println("Unable to load images!");
            System.exit(-1);
        }

        // tease out the fonts to use, based on existing default font of the
        // graphics context
        Graphics g = new BufferedImage(50, 50, BufferedImage.TYPE_INT_RGB).getGraphics();
        numberFont = new Font(g.getFont().toString(), 0, 20);
        noteFont = new Font(g.getFont().toString(), 0, 10);

        g.dispose();

        // set initial image and number value
        image = normal;
        num = "";

        // pay attention to the mouse, so we can change images as 
        // mouse enters and exits
        this.addMouseListener(this);

        // initialize notes, selected, given flags
        notesMode = false;
        isSelected = false;
        isGiven = false;

//        System.out.println("n " + normal);
//        System.out.println("s " + selected);
//        System.out.println("t " + noteImg);
    }

    /**
     * Turn on notes-entering mode.
     */
    public void setNotesMode() {
        notesMode = true;
    }

    /**
     * Turn off notes-entering mode.
     */
    public void setNormalMode() {
        notesMode = false;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // do nothing on clicks
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // do nothing on presses
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // do nothing on releases
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // when the mouse is over the cell, update selected flag
        // and repaint the cell
        isSelected = true;
        repaint();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // when the mouse exits the cell, update selected flag
        // and repaint the cell
        isSelected = false;
        repaint();
    }

    /**
     * Is this cell selected? I.e., is the mouse pointer over the cell?
     *
     * @return True if the cell is selected, false otherwise.
     */
    public boolean isSelected() {
        return isSelected;
    }

    /**
     * Set the number to display in this cell. Do nothing if the cell is a
     * given.
     *
     * @param number Number in [0, 9] to place in the cell. 0 means erase the
     * current number, [1, 9] means place value in cell.
     */
    public void setNumber(char number) {
        if (!isGiven) {
            if (number == '0') {
                num = "";
            } else {
                num = Character.toString(number);
            }
        }
        repaint();
    }

    /**
     * Toggle the note status for a number in the cell.
     *
     * @param number Note to set/unset in the cell.
     */
    public void setNote(char number) {
        int i = Integer.parseInt(Character.toString(number));
        notes[i] = !notes[i];
        repaint();
    }

    /**
     * Place a number in the cell as a given.
     *
     * @param number Given to place in the cell, as a char.
     */
    public void setGiven(char number) {
        isGiven = true;
        num = Character.toString(number);
    }

    /**
     * Remove the "given" status of the cell.
     */
    public void unsetGiven() {
        isGiven = false;
    }

    /**
     * Remove all notes from the cell.
     */
    public void clearAllNotes() {
        for (int i = 0; i < notes.length; i++) {
            notes[i] = false;
        }
        repaint();
    }

    /**
     * Remove the number from a cell.
     */
    public void clearNumber() {
        num = "";
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        // determine which image to display, then show it
        if (blockSolved) {
            this.setBackground(bitColor);
        } else {
            if (isSelected) {
                image = notesMode ? noteImg : selected;
            } else {
                image = normal;
            }

            g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), this);

        }

        int w = this.getWidth();
        int h = this.getHeight();

        // paint the cell's number in the center
        g.setColor(isGiven ? Color.BLACK : Color.BLUE);
        g.setFont(numberFont);
        g.drawString(num, w / 2, h / 2 + 10);

        // paint the notes around the edge of the cell
        g.setColor(Color.RED);
        g.setFont(noteFont);
        if (notes[1]) {
            g.drawString("1", 4, 4 * h / 5);
        }
        if (notes[2]) {
            g.drawString("2", 4, h / 2);
        }
        if (notes[3]) {
            g.drawString("3", 4, h / 5);
        }

        if (notes[4]) {
            g.drawString("4", w / 6, 10);
        }
        if (notes[5]) {
            g.drawString("5", w / 2, 10);
        }
        if (notes[6]) {
            g.drawString("6", 5 * w / 6, 10);
        }

        if (notes[7]) {
            g.drawString("7", w - 8, h / 5);
        }
        if (notes[8]) {
            g.drawString("8", w - 8, h / 2);
        }
        if (notes[9]) {
            g.drawString("9", w - 8, 4 * h / 5);
        }
    }

    public void update(Observable obs, Object arg) {
        if (obs instanceof BlockObservable) {
            BlockObservable block = (BlockObservable) obs;
            this.blockSolved = block.getSolved();
            this.repaint();
        }
    }

    public void setBitColor(String clr) {
        bitColor = Color.decode(clr);
    }

    public void setBlockObservable(Observable o) {
        blockObservable = o;
        blockObservable.addObserver(this);
    }
    
    public void setSolved(boolean b){
        this.blockSolved = b;
    }
}
