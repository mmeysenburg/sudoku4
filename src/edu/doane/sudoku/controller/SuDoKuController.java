package edu.doane.sudoku.controller;

/**
 * Interface for the controller in the SuDoKu MVC architecture.
 * 
 * @author Mark M. Meysenburg
 * @version 12/15/2015
 */
public interface SuDoKuController {
	
    /**
     * Play a number at the specified location.
     * 
     * @param row Row to place the number in.
     * @param col Column to place the number in.
     * @param number Number to place in the specified location.
     */
    public void playNumber(int row, int col, int number);
    
    /**
     * Remove the number from the specified location. If the number there is a 
     * given, or there is no number yet, do nothing.
     * 
     * @param row Row to place the number in.
     * @param col Column to place the number in.
     */
    public void removeNumber(int row, int col);
    
    /**
     * Set / unset a note at the specified location.
     * 
     * @param row Row to set the note in.
     * @param col Column to set the note in.
     * @param number Note to toggle in the specified location.
     */
    public void setNote(int row, int col, int number);

    /**
     * Request a game of a specified difficulty.
     * 
     * @param difficulty String specifying the difficulty of game to fetch.
     */
    public void requestGame(String difficulty);  
    
    /**
     * Shut the game down in an orderly fashion.
     */
    public void shutDown();
    
    /**
     * Cause the view to display the "About Doane SuDoKu" modal dialog.
     */
    public void displayAbout();
    
    /**
     * Remove everything from the view grid.
     */
    public void clearViewGrid();
    
    /**
     * Cause the view to display the "Instructions Doane SuDoku" model dialog.
     */
    public void displayInstructions();
    
    /**
     * Remove all non-given numbers, and all notes, from both the
     * model and view grids. 
     */
    public void resetGrids();
    
    /**
     * Set / unset paused mode
     * 
     * @param paused variable to show whether the game is currently paused or not
     */
    public void isPaused(boolean paused);
    
    public BlockObservable getBlockObservable(int i);
 
    public boolean getIsLocked();
    
    public SuDoKuTimer getTimer();
    
      
}
