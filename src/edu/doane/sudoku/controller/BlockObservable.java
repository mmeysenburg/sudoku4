/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.doane.sudoku.controller;

import java.util.Observable;
/**
 *
 * @author Ashby
 */
public class BlockObservable extends Observable{
    
    private boolean solved = false;
    
    public void setSolved(boolean b){
        if(b != solved){
            solved = b;
            setChanged();
            notifyObservers(solved);
        }else{
            
        }
    }
    
    public boolean getSolved(){
        return solved;
    }
    
}
